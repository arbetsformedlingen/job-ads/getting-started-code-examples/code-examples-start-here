# Getting started - code examples 

Code examples for using Jobtech APIs.

### Which API should I use?
* Do you want to keep a copy of all Arbetsförmedlingen's ads, or all ads filtered by geographical areas or occupation?  
 Then you should use JobStream.
  

* Do you want to be able to make searches and only get a limited amount of Arbetsförmedlingen's ads that match the queries?
In that case JobSearch is more suitable.  
  

* Do you want to do specific queries on ads from external ad providers? 
 Use the JobTech Links API.
  

* Do you want all ads from external ads providers?
For that scenario we don't have an API, but the files with all ads are available at: https://data.jobtechdev.se/annonser/jobtechlinks/index.html


### JobSearch
A search API for all currently published ads at Arbetsförmedlingen/Platsbanken.<br />
API here: https://jobsearch.api.jobtechdev.se <br />
Repository here: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis

Example file: `jobsearch_examples.py` 

JobSearch can return a maximum of 100 ads at the time, and will require additional searches to get more results from the same query.


### JobStream
If you want to download all ads, rather than searching for specific ads, it's JobStream you should use.
It's exactly the same data set as JobSearch, and it has an endpoint for getting real-time updates of changes.

A code example for JobStream is found in a different repository: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/jobstream-example

### JobTech Links
This is a larger data set including also ads from external job ad providers. 
However, there is less information per ad. E.g. the text body of the ad is lacking, instead there is a brief description.<br />
API here:  https://links.api.jobtechdev.se <br />
Repository here: https://gitlab.com/arbetsformedlingen/job-ads/joblinks-search-api

Example file: `jobtechlinks_examples.py` 

### Frontend example
Here is an example of how to write a frontend to our APIs. This example is for JobTech Links.
https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/frontend-example

## Getting started with Python

Prerequisites: Python 3.10+<br />
Install python packages with: `pip install -r requirements.txt`

